Mass Image Import
=================

This module implements a bulk image import operation that doesn't have a limit
of 50 images at a time as happens with the image_import module.

It does so by using a different batch operation and by not relying in a image
form selection.

This module is a sollution to problems such as the one reported at
http://drupal.org/node/493686

Usage
-----

Just drop the images you want to import in the image import folder (configured
by the image_import module) and go to Admin -> Content -> Mass image import.
Make sure the webserver has enough rights to control the image files.

Then a batch operation will take care of importing all your images.
